<?php
  // header('Access-Control-Allow-Origin: *');
  // header("Access-Control-Allow-Methods: GET, OPTIONS");
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Home Page</title>
    <!-- Bootstrap core CSS-->
    <!-- <?php echo link_tag('assests/vendor/bootstrap/css/bootstrap.min.css'); ?> -->
    <!-- Custom fonts for this template-->
    <!-- <?php echo link_tag('assests/vendor/fontawesome-free/css/all.min.css'); ?> -->
    <!-- Page level plugin CSS-->
    <!-- <?php echo link_tag('assests/vendor/datatables/dataTables.bootstrap4.css'); ?> -->
    <!-- Custom styles for this template-->
    <!-- <?php echo link_tag('assests/css/sb-admin.css'); ?> -->

    <!-- template part -->
    <?php echo link_tag('https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,700,800'); ?>
    <?php echo link_tag('assests/css/open-iconic-bootstrap.min.css'); ?>
    <?php echo link_tag('assests/css/animate.css'); ?>
    <?php echo link_tag('assests/css/owl.carousel.min.css'); ?>
    <?php echo link_tag('assests/css/owl.theme.default.min.css'); ?>
    <?php echo link_tag('assests/css/magnific-popup.css'); ?>
    <?php echo link_tag('assests/css/aos.css'); ?>
    <?php echo link_tag('assests/css/ionicons.min.css'); ?>
    <?php echo link_tag('assests/css/bootstrap-datepicker.css'); ?>
    <?php echo link_tag('assests/css/jquery.timepicker.css'); ?>
    <?php echo link_tag('assests/css/flaticon.css'); ?>
    <?php echo link_tag('assests/css/icomoon.css'); ?>
    <?php echo link_tag('assests/css/style.css'); ?>

</head>
<style>
  @media (min-width: 1199px) {
    .overlay{
      background-image: url('/assests/template/img/ref/bg1-min.png')!important;
    }
  }
  /* @media (max-width: 768px) {
    .block-3 {
        margin-bottom: 7em;
    }
    section.ftco-section-featured.ftco-animate.fadeInUp.ftco-animated {
        display: none;
    }
} */
@media (max-width: 1200px) {
    .nav-link {
        padding: 0.5rem 0.3rem;
    }
    .navbar-expand-lg .navbar-nav .nav-link {
        padding-right: 0.5rem;
        padding-left: 0.5rem;
    }
    .ftco-navbar-light .navbar-nav>.nav-item>.nav-link {
    font-size: 12px;}
}
.nav-mob{
  display:none;
}
@media (max-width: 991px) {
.nav-web{
  display:none;
}
.nav-mob{
  display:block;
}

}
span.small-title,
span.middle-title {
    font-size: 18px;
    color: #9da454;
    font-weight: 500;
}
p.subtitle {
    font-size: 25px;
}
</style>
<body>


    <div class="col-md-10" style="margin: auto; padding: 0;">
          <nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
              <div class="container">
                  <a class="navbar-brand" href="/home">Revolver</a>
                  <a class="nav-link nav-web" style="color: #f0f0f0; font-weight: 600;" href="/home">Concierge</a>
                  <a class="nav-link nav-web" style="color: #f0f0f0; font-weight: 600;" href="/home">Itineraries</a>
                  <a class="nav-link nav-web" style="color: #f0f0f0; font-weight: 600;" href="/home">Experiences</a>
                  <a class="nav-link nav-web" style="color: #f0f0f0; font-weight: 600;" href="/home">Offers</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav"
                      aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="oi oi-menu"></span> Menu
                  </button>

                  <div class="collapse navbar-collapse" id="ftco-nav">
                      <ul class="navbar-nav ml-auto">
                          <li class="nav-item nav-mob"><a class="nav-link" style="color: #f0f0f0; font-weight: 600;" href="/home">Concierge</a></li>
                          <li class="nav-item nav-mob"><a class="nav-link" style="color: #f0f0f0; font-weight: 600;" href="/home">Itineraries</a></li>
                          <li class="nav-item nav-mob"><a class="nav-link" style="color: #f0f0f0; font-weight: 600;" href="/home">Experiences</a></li>
                          <li class="nav-item nav-mob"><a class="nav-link" style="color: #f0f0f0; font-weight: 600;" href="/home">Offers</a></li>
                          <li class="nav-item"><a href="#" class="nav-link" style="font-weight: 600;">Become a Vendor</a></li>
                          <li class="nav-item"><a href="#" class="nav-link" style="font-weight: 600;">Help</a></li>
                          <li class="nav-item"><a href="<?php echo site_url('/user/signup')?>" class="nav-link" style="font-weight: 600;"><span>Sign
                                      Up</span></a></li>
                          <li class="nav-item"><a href="<?php echo site_url('/user/login')?>" class="nav-link" style="font-weight: 600;"><span>Sign
                                      In</span></a></li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="hero-wrap js-fullheight wave-container">

              <div class="overlay"></div>
              <!-- <div id="particles-js"></div> -->

          </div>
          
          <div class="bg-light">
              <section class="ftco-section-featured ftco-animate">
                  <div class="container-fluid" data-scrollax-parent="true">
                      <div class="row no-gutters d-flex align-items-center"
                          data-scrollax=" properties: { translateY: '-30%'}">
                        
                            <div class="row">
                              <div class="col-md-3 col-md-offset-2 avart-1" style="text-align: center;">
                                  <img src="<?= base_url('../assests/template/img/ref/avarta_1.png');?>" width="150" height="150" alt="">
                                  <h4 class="subtitle"><b>Reynard</b></h4>
                              </div> 

                              <div class="col-md-3" style="line-height: 1.2!important;vertical-align: middle;">
                                <h5 style="padding-top: 10px;">
                                "From the weekend get-away to extended stays. TRL's travel tools & localized offers make planning trips a breeze!"<br></h5>
                                <span class="small-title">#makeithappen</span>
                              </div>
                            </div>
                        
                          <!-- <div class="col-md-12">
                              <a href="#" class="featured-img">
                                  
                                  <img src="<?php echo base_url('assests/images/image_1.jpg'); ?>" class="img-fluid" alt="">
                                  
                              </a>
                          </div> -->


                      </div>

                  </div>
              </section>
          </div>
          <section class="ftco-section">
              <div class="heading-about">
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="wow bounceInDown" data-wow-delay="0.4s">
                                  <div class="section-heading">
                                      <h3>Latest Offers</h3>
                                  </div>
                                  <hr class="marginbot-50">
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="container">
                  <div class="row">
                      <div class="col-md-3">
                          <div class="wow bounceInUp" data-wow-delay="0.2s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_11.png');?> "
                                          style="width: 100%;" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="small-title">Family.barcelona</span>
                                      <p class="subtitle">Barcelona Aquarium Fast-Track E-Ticket</p>
                                      <span class="small-price">$15&nbsp&nbsp per&nbsp&nbsp person</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="wow bounceInUp" data-wow-delay="0.5s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_22.png');?>"
                                          style="width: 100%;" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="small-title">music.barcelona</span>
                                      <p class="subtitle">Best Beach Party at the W Hotel</p>
                                      <span class="small-price">$35&nbsp&nbsp per&nbsp&nbsp person</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="wow bounceInUp" data-wow-delay="0.8s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_33.png');?>"
                                          style="width: 100%;" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="small-title">tour.barcelona</span>
                                      <p class="subtitle">Sagrada Family Tour-Skip the line</p>
                                      <span class="small-price">$40&nbsp&nbsp per&nbsp&nbsp person</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="wow bounceInUp" data-wow-delay="1s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_44.png');?>"
                                          style="width: 100%;" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="small-title">ride.barcelona</span>
                                      <p class="subtitle">Barcelona Bus Tours around the City</p>
                                      <span class="small-price">$15&nbsp&nbsp per&nbsp&nbsp person</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                  </div>
              </div>

          </section>
          <section class="ftco-section testimony-section bg-light">
            <div class="heading-about">
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="wow bounceInDown" data-wow-delay="0.4s">
                                  <div class="section-heading">
                                      <h3>Best Experiences</h3>
                                  </div>
                                  <hr class="marginbot-50">
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="container">

                  <style>
                  .owl-dots{
                    display:none;
                  }
                  </style>

                  <div class="row ftco-animate">
                    <div class="col-md-12">
                      <div class="carousel-testimony owl-carousel ftco-owl">
                        <div class="item text-center">
                          <div class="testimony-wrap p-4 pb-5">
                                                <div class="wow bounceInUp" data-wow-delay="0.5s">
                                                    <div class="inner">
                                                        <div class="avatar"><img
                                                                src="<?= base_url('assests/template/img/ref/con_66.png');?>"
                                                                style="width: 100%;" alt="" class="img-responsive" />
                                                        </div>
                                                        <div>
                                                            <span class="small-title">nightlife.barcelona</span>
                                                            <p class="subtitle">Touring the mountains around Barcelona</p>
                                                            <span class="small-price">$50&nbsp&nbsp per&nbsp&nbsp person</span>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
                        </div>
                        <div class="item text-center">
                          <div class="testimony-wrap p-4 pb-5">
                                                <div class="wow bounceInUp" data-wow-delay="0.5s">
                                                    <div class="inner">
                                                        <div class="avatar"><img
                                                                src="<?= base_url('assests/template/img/ref/con_77.png');?>"
                                                                style="width: 100%;" alt="" class="img-responsive" />
                                                        </div>
                                                        <div>
                                                            <span class="small-title">nightlife.barcelona</span>
                                                            <p class="subtitle">Touring the mountains around Barcelona</p>
                                                            <span class="small-price">$50&nbsp&nbsp per&nbsp&nbsp person</span>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
                        </div>
                        <div class="item text-center">
                          <div class="testimony-wrap p-4 pb-5">
                                                <div class="wow bounceInUp" data-wow-delay="0.5s">
                                                    <div class="inner">
                                                        <div class="avatar"><img
                                                                src="<?= base_url('assests/template/img/ref/con55.png');?>"
                                                                style="width: 100%;" alt="" class="img-responsive" />
                                                        </div>
                                                        <div>
                                                            <span class="small-title">nightlife.barcelona</span>
                                                            <p class="subtitle">Touring the mountains around Barcelona</p>
                                                            <span class="small-price">$50&nbsp&nbsp per&nbsp&nbsp person</span>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
                        </div>
                        <div class="item text-center">
                          <div class="testimony-wrap p-4 pb-5">
                                                <div class="wow bounceInUp" data-wow-delay="0.5s">
                                                    <div class="inner">
                                                        <div class="avatar"><img
                                                                src="<?= base_url('assests/template/img/ref/con_66.png');?>"
                                                                style="width: 100%;" alt="" class="img-responsive" />
                                                        </div>
                                                        <div>
                                                            <span class="small-title">nightlife.barcelona</span>
                                                            <p class="subtitle">Touring the mountains around Barcelona</p>
                                                            <span class="small-price">$50&nbsp&nbsp per&nbsp&nbsp person</span>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
                        </div>
                        <div class="item text-center">
                          <div class="testimony-wrap p-4 pb-5">
                                                <div class="wow bounceInUp" data-wow-delay="0.5s">
                                                    <div class="inner">
                                                        <div class="avatar"><img
                                                                src="<?= base_url('assests/template/img/ref/con_77.png');?>"
                                                                style="width: 100%;" alt="" class="img-responsive" />
                                                        </div>
                                                        <div>
                                                            <span class="small-title">nightlife.barcelona</span>
                                                            <p class="subtitle">Touring the mountains around Barcelona</p>
                                                            <span class="small-price">$50&nbsp&nbsp per&nbsp&nbsp person</span>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
                        </div>
                        <div class="item text-center">
                          <div class="testimony-wrap p-4 pb-5">
                                                <div class="wow bounceInUp" data-wow-delay="0.5s">
                                                    <div class="inner">
                                                        <div class="avatar"><img
                                                                src="<?= base_url('assests/template/img/ref/con55.png');?>"
                                                                style="width: 100%;" alt="" class="img-responsive" />
                                                        </div>
                                                        <div>
                                                            <span class="small-title">nightlife.barcelona</span>
                                                            <p class="subtitle">Touring the mountains around Barcelona</p>
                                                            <span class="small-price">$50&nbsp&nbsp per&nbsp&nbsp person</span>
                                                        </div>
                                                    </div>
                                                </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  
              </div>
          </section>
          <section class="ftco-section">
              <div class="heading-about">
                  <div class="container">
                      <div class="row">
                          <div class="col-lg-12">
                              <div class="wow bounceInDown" data-wow-delay="0.4s">
                                  <div class="section-heading">
                                      <h3>Top Nomad-Friendly Cities</h3>
                                  </div>
                                  <hr class="marginbot-50">
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
              <div class="container">

                  <div class="row">
                      <div class="col-md-2">
                          <div class="wow bounceInUp" data-wow-delay="0.3s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_1_1.png');?>"
                                          style="width:100%" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="middle-title">Bali</span>

                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="wow bounceInUp" data-wow-delay="0.8s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_1-2.png');?>"
                                          style="width:100%" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="middle-title">Valencia</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="wow bounceInUp" data-wow-delay="1.2s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_1_3.png');?>"
                                          style="width:100%" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="middle-title">Chang Mai</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="wow bounceInUp" data-wow-delay="1s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_1_4.png');?>"
                                          style="width:100%" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="middle-title">Johor Bahru</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="wow bounceInUp" data-wow-delay="0.6s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_1_5.png');?>"
                                          style="width:100%" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="middle-title">Koh Samui</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>
                      <div class="col-md-2">
                          <div class="wow bounceInUp" data-wow-delay="0.4s">
                              <!-- <div class="team boxed-grey"> -->
                              <div class="inner">
                                  <div class="avatar"><img src="<?= base_url('assests/template/img/ref/con_1_6.png');?>"
                                          style="width:100%" alt="" class="img-responsive" /></div>
                                  <div>
                                      <span class="middle-title">Havana</span>
                                  </div>
                              </div>
                              <!-- </div> -->
                          </div>
                      </div>

                  </div>
              </div>
          </section>
          <footer class="ftco-footer ftco-bg-dark ftco-section">
          
              <div class="container" >
              <hr class="marginbot-20">

                  <div class="row">
                      <div class="col-md">
                          <div class="ftco-footer-widget">
                              <h2 class="ftco-heading-2">Localization Preferences</h2>
                              <p></p>
                          </div>
                      </div>
                      <div class="col-md">
                          <div class="ftco-footer-widget">
                              <h2 class="ftco-heading-2">The Revolver Life</h2>
                              <ul class="list-unstyled">
                                    <li><a href="#">About us</a></li>
                                    <li><a href="#">Careers</a></li>
                                    <li><a href="#">Press</a></li>
                                    <li><a href="#">Policies</a></li>
                                    <li><a href="#">Help</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="col-md">
                          <div class="ftco-footer-widget">
                              <h2 class="ftco-heading-2">Discover</h2>
                              <ul class="list-unstyled">
                                    <li><a href="#">Trust & Safety</a></li>
                                    <li><a href="#">Travel Credit</a></li>
                                    <li><a href="#">Gidt Cards</a></li>
                                    <li><a href="#">Become a Vendor</a></li>
                              </ul>
                          </div>
                      </div>
                      <div class="col-md">
                          <div class="ftco-footer-widget">
                              <h2 class="ftco-heading-2">Vendors</h2>
                              <ul class="list-unstyled">
                                  <li><a href="#">Why be a Vendor</a></li>
                                  <li><a href="#">Expectations</a></li>
                                  <li><a href="#">Responsibility Code</a></li>
                                  <li><a href="#">Community Center</a></li>
                              </ul>
                          </div>
                      </div>
                  </div>
                  <hr class="marginbot-20">

                  <div class="row">
                      <div class="col-md-12 text-center">
                        <div class="row">
                                <div class="col-md-6"> 
                                      <a href="#" class="footer-copy"> Copyright &copy;<script>
                                      document.write(new Date().getFullYear());
                                      </script> The Revolver Lif, Inc.</a>
                                </div>
                                <div class="col-md-6 footer-icons">
                                  <a href="#a">Terms</a>
                                  <a href="#b">Privacy</a>
                                  <a href="#c">Site Map</a>
                                  <a href="#d"><span class="icon-facebook"></span></a>
                                  <a href="#"><span class="icon-twitter"></span></a>
                                  <a href="#"><span class="icon-instagram"></span></a>
                                </div>
                                <div class="col-md-6 footer-icons-web">
                                  <a href="#"><span class="icon-instagram"></span></a>
                                  <a href="#"><span class="icon-twitter"></span></a>
                                  <a href="#"><span class="icon-facebook"></span></a>
                                  <a href="#">Site Map</a>
                                  <a href="#">Privacy</a>
                                  <a href="#">Terms</a>
                                  
                                  
                                  
                                  
                                  
                                </div>
                        </div>
                      </div>
                  </div>
              </div>
          </footer>

    </div>

    <!-- END nav -->
    <!-- <div class="js-fullheight"> -->

    <!-- loader -->
    <div id="ftco-loader" class="show fullscreen"><svg class="circular" width="48px" height="48px">
            <circle class="path-bg" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke="#eeeeee" />
            <circle class="path" cx="24" cy="24" r="22" fill="none" stroke-width="4" stroke-miterlimit="10"
                stroke="#F96D00" /></svg></div>



    <!-- <script src="<?php echo base_url('assests/vendor/jquery/jquery.min.js'); ?>"></script> -->
    <!-- <script src="<?php echo base_url('assests/vendor/bootstrap/js/bootstrap.bundle.min.js'); ?>"></script> -->
    <!-- Core plugin JavaScript-->
    <!-- <script src="<?php echo base_url('assests/vendor/jquery-easing/jquery.easing.min.js'); ?>"></script> -->

    <!-- Page level plugin JavaScript-->
    <!-- <script src="<?php echo base_url('assests/vendor/chart.js/Chart.min.js'); ?>"></script> -->
    <!-- <script src="<?php echo base_url('assests/vendor/datatables/jquery.dataTables.js'); ?>"></script> -->
    <!-- <script src="<?php echo base_url('assests/vendor/datatables/dataTables.bootstrap4.js'); ?>"></script> -->

    <!-- Custom scripts for all pages-->
    <!-- <script src="<?php echo base_url('assests/js/sb-admin.min.js'); ?>"></script> -->
    <!-- <script src="<?php echo base_url('assests/js/demo/datatables-demo.js'); ?>"></script> -->
    <!-- <script src="<?php echo base_url('assests/js/demo/chart-area-demo.js'); ?>"></script> -->

    <!-- template part -->
    <script src="<?php echo base_url('assests/js/jquery.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/jquery-migrate-3.0.1.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/popper.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/bootstrap.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/jquery.easing.1.3.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/jquery.waypoints.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/owl.carousel.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/aos.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/jquery.animateNumber.min.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/bootstrap-datepicker.js'); ?>"></script>
    <script src="<?php echo base_url('assests/js/particles.min.js'); ?>"></script>
    <!-- <script src="<?php echo base_url('assests/js/particle.js'); ?>"></script> -->
    <!-- <script src="<?php echo base_url('https://maps.googleapis.com/maps/api/js?key=AIzaSyBVWaKrjvy3MaE7SQ74_uJiULgl1JY0H2s&sensor=false'); ?>"></script> -->
    <script src="<?php echo base_url('assests/js/scrollax.min.js'); ?>"></script>
    <!-- <script src="<?php echo base_url('assests/js/google-map.js'); ?>"></script> -->
    <script src="<?php echo base_url('assests/js/main.js'); ?>"></script>

</body>

</html>