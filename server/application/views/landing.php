<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>Revoler</title>

  <!-- Bootstrap Core CSS -->
  <link href="<?= base_url('../assests/template/css/bootstrap.min.css');?>" rel="stylesheet" type="text/css">

  <!-- Fonts -->
  <link href="<?= base_url('../assests/template/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet" type="text/css">
  <link href="<?= base_url('../assests/template/css/animate.css');?>" rel="stylesheet" />
  <!-- Squad theme CSS -->
  <link href="<?= base_url('../assests/template/css/style.css');?>" rel="stylesheet">
  <link href="<?= base_url('../assests/template/color/default.css');?>" rel="stylesheet">

  <!-- =======================================================
    Theme Name: Squadfree
    Theme URL: https://bootstrapmade.com/squadfree-free-bootstrap-template-creative/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
  <!-- Preloader -->
  <div id="preloader">
    <div id="load"></div>
  </div>

  <div class="row">
    <div class="col-md-10 col-md-offset-1">

  <nav class="navbar navbar-custom navbar-fixed-top" role="navigation">
    <div class="container">
      <!-- <div class="navbar-header page-scroll"> -->
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i class="fa fa-bars"></i>
                </button>
        <a class="navbar-brand" href="index.html">
          <!-- <h1>SQUAD FREE</h1> -->
        </a>
      <!-- </div> -->
      <div class="collapse navbar-collapse navbar-right navbar-main-collapse">
      <ul class="nav navbar-nav">
        <li class="active"><a href="">Concierge</a></li>
        <li><a href="">Itineraries</a></li>
        <li><a href="">Experiences</a></li>
        <li><a href="">Offers</a></li>

          <li><a href="">Become a Vender</a></li>
          <li><a href="">Help</a></li>
          <li><a href="">SignUp</a></li>
          <li><a href="">SginIn</a></li>

        </ul></div>
      <!-- </div> -->
      <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
  </nav>

  <!-- Section: intro -->
  <section id="intro" class="intro">
    <div class="row">
      <div class="col-md-8 col-md-offset-2"style="top: 40%;">
        <p class="p-2">Have more experiences </p>
        <p class="p-2">tell more stories...</p>

        <form method="post" action="" style="background-color: white; border-radius: 10px; height: 70px; margin-top: 40px;">
          <div class="row">
            <div class="col-md-6 add-style">
              <i class="fa fa-search" style="margin-left: 20px; font-size: 20px; color: #9da454;"></i>
              <input type="text" placeholder='Try "Barcelona"' name="" value="" style="padding: 0 20px; font-weight: bold; color: rgba(20, 20, 20, 0.7)" />
            </div>
            <div class="col-md-6 add-style">
              <div class="row">
                <div class="col-md-4">
                  <p style="margin: 0; font-size: 10px; font-weight: bold;">FROM</p>
                  <i class="fa fa-calendar-o" style="margin-left: 5px; font-size: 20px; color: #9da454;"></i>
                </div>
                <div class="col-md-4">
                  <p style="margin: 0; font-size: 10px; font-weight: bold;">TO</p>
                  <i class="fa fa-calendar-o" style="font-size: 20px; color: #9da454;"></i>
                </div>
                <div class="col-md-4">
                  <input type="submit" style="background-color: #333; border: 3px solid #997300; border-radius: 5px; color:#997300; padding: 0 15px;" value="Search">
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>

    <div class="page-scroll">
      <a href="#about" class="btn btn-circle">
				<i class="fa fa-angle-double-down animated"></i>
			</a>
    </div>
<!--
  <div class="wave">
  </div> -->

</section>
<div class="avarta">
  <div class="row">
    <div class="col-md-4 col-md-offset-2 avart-1" style="text-align: center;">
        <img src="<?= base_url('../assests/template/img/ref/avarta_1.png');?>" width="150" height="150" alt="">
        <h4 class="subtitle"><b>Reynard</b></h4>
    </div>

    <div class="col-md-6" style="line-height: 1.2!important;vertical-align: middle;">
      <h5 style="padding-top: 10px;">
      "From the weekend get-away to extended stays. TRL's travel tools & localized offers make planning trips a breeze!"<br></h5>
      <span class="small-title">#makeithappen</span>
    </div>
  </div>
</div>
  <!-- /Section: intro -->

  <!-- Section: about -->
  <section id="about" class="home-section text-left">
    <div class="heading-about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="wow bounceInDown" data-wow-delay="0.4s">
              <div class="section-heading">
                <h3>Latest Offers</h3>
              </div>
              <hr class="marginbot-50">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">

      <div class="row">
        <div class="col-md-3">
          <div class="wow bounceInUp" data-wow-delay="0.2s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_11.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="small-title">Family.barcelona</span>
                  <p class="subtitle">Barcelona Aquarium Fast-Track E-Ticket</p>
                  <span class="small-price">$15&nbsp&nbsp per&nbsp&nbsp person</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-3">
          <div class="wow bounceInUp" data-wow-delay="0.5s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_22.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="small-title">music.barcelona</span>
                  <p class="subtitle">Best Beach Party at the W Hotel</p>
                  <span class="small-price">$35&nbsp&nbsp per&nbsp&nbsp person</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-3">
          <div class="wow bounceInUp" data-wow-delay="0.8s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_33.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="small-title">tour.barcelona</span>
                  <p class="subtitle">Sagrada Family Tour-Skip the line</p>
                  <span class="small-price">$40&nbsp&nbsp per&nbsp&nbsp person</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-3">
          <div class="wow bounceInUp" data-wow-delay="1s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_44.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="small-title">ride.barcelona</span>
                  <p class="subtitle">Barcelona Bus Tours around the City</p>
                  <span class="small-price">$15&nbsp&nbsp per&nbsp&nbsp person</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- /Section: about -->
  <section id="about1" class="home-section text-left">
    <div class="heading-about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="wow bounceInDown" data-wow-delay="0.4s">
              <div class="section-heading">
                <h3>Best Experiences</h3>
              </div>
              <hr class="marginbot-50">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row">
        <div class="col-md-4">
          <div class="wow bounceInUp" data-wow-delay="0.2s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con55.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="small-title">park.barcelona</span>
                  <p class="subtitle">Park Guell, what a treat!</p>
                  <span class="small-price">$15&nbsp&nbsp per&nbsp&nbsp person</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-4">
          <div class="wow bounceInUp" data-wow-delay="0.5s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_66.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="small-title">nightlife.barcelona</span>
                  <p class="subtitle">Touring the mountains around Barcelona</p>
                  <span class="small-price">$50&nbsp&nbsp per&nbsp&nbsp person</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-4">
          <div class="wow bounceInUp" data-wow-delay="0.8s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_77.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="small-title">culture.barcelona</span>
                  <p class="subtitle">3 Days in Barcelona</p>
                  <span class="small-price">$100&nbsp&nbsp per&nbsp&nbsp person</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>


      </div>
    </div>
  </section>

  <section id="about2" class="home-section text-left">
    <div class="heading-about">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <div class="wow bounceInDown" data-wow-delay="0.4s">
              <div class="section-heading">
                <h3>Top Nomad-Friendly Cities</h3>
              </div>
              <hr class="marginbot-50">
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="container">

      <div class="row">
        <div class="col-md-2">
          <div class="wow bounceInUp" data-wow-delay="0.3s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_1_1.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="middle-title">Bali</span>

                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-2">
          <div class="wow bounceInUp" data-wow-delay="0.8s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_1-2.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="middle-title">Valencia</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-2">
          <div class="wow bounceInUp" data-wow-delay="1.2s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_1_3.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="middle-title">Chang Mai</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-2">
          <div class="wow bounceInUp" data-wow-delay="1s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_1_4.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="middle-title">Johor Bahru</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-2">
          <div class="wow bounceInUp" data-wow-delay="0.6s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_1_5.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="middle-title">Koh Samui</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>
        <div class="col-md-2">
          <div class="wow bounceInUp" data-wow-delay="0.4s">
            <!-- <div class="team boxed-grey"> -->
              <div class="inner">
                <div class="avatar"><img src="<?= base_url('../assests/template/img/ref/con_1_6.png');?>" alt="" class="img-responsive" /></div>
                <div>
                  <span class="middle-title">Havana</span>
                </div>
              </div>
            <!-- </div> -->
          </div>
        </div>

      </div>
    </div>
  </section>
  <br><br><br>
  <hr class="marginbot-50">
  <div>
    <div class="container">
      <div class="row">
        <div class="col-md-3">
          <div class="f-header">
            Localization Preferences
          </div>
          <option>
          </option>
        </div>
        <div class="col-md-3">
          <div class="f-header">
            The Revolver Life
          </div>
          <ul class="nav nav-bar">
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">About us</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Careers</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Press</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Policies</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Help</a></li>
          </ul>
        </div>
        <div class="col-md-3">
          <div class="f-header">
            Discover
          </div>
          <ul class="nav nav-bar">
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Trust & Safety</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Travel Credit</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Gidt Cards</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Become a Vendor</a></li>

          </ul>
        </div>
        <div class="col-md-3">
          <div class="f-header">
            Vendors
          </div>
          <ul class="nav nav-bar">
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Why be a Vendor</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Expectations</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Responsibility Code</a></li>
            <li class="nav-item"><a href="" style="padding: 5px 15px; color: #333;">Community Center</a></li>

          </ul>
        </div>
      </div>
      <hr class="marginbot-20">

        <ul class="nav navbar-nav" float="left">
          <li><a href="#intro"  style="padding: 5px 15px; color: #333;"><i class="fa fa-copy"></i>The Revolver Lif, Inc.</a></li>
        </ul>
        <ul class="nav navbar-nav" style="float:right;">
          <li><a href="#intro" style="padding: 5px 15px; color: #333;">Terms</a></li>
          <li><a href="#about" style="padding: 5px 15px; color: #333;">Privacy</a></li>
          <li><a href="#service" style="padding: 5px 15px; color: #333;">Site Map</a></li>
          <li><a href="#contact" style="padding: 5px 15px; color: #333;"><i class="fa fa-facebook"></i></a></li>
          <li><a href="#contact" style="padding: 5px 15px; color: #333;"><i class="fa fa-twitter"></i></a></li>
          <li><a href="#contact" style="padding: 5px 15px; color: #333;"><i class="fa fa-instagram"></i></a></li>
        </ul>
        <br>
    </div>
  </div>
</div></div>
  <!-- Core JavaScript Files -->
  <script src="<?= base_url('../assests/template/js/jquery.min.js');?>"></script>
  <script src="<?= base_url('../assests/template/js/bootstrap.min.js');?>"></script>
  <script src="<?= base_url('../assests/template/js/jquery.easing.min.js');?>"></script>
  <script src="<?= base_url('../assests/template/js/jquery.scrollTo.js');?>"></script>
  <script src="<?= base_url('../assests/template/js/wow.min.js');?>"></script>
  <!-- Custom Theme JavaScript -->
  <script src="<?= base_url('../assests/template/js/custom.js');?>"></script>
  <script src="<?= base_url('../assests/template/contactform/contactform.js');?>"></script>

</body>

</html>
